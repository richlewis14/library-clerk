class AddPaperclipToBookModel < ActiveRecord::Migration
  def change
    add_attachment :books, :avatar
  end

  def down
    remove_attachment :books, :avatar
  end
end
