class AddNameToCategoryModel < ActiveRecord::Migration
  def change

    add_column :categories, :name, :string
  end

  def down

    remove_column :categories, :name

  end
end
