class AddColumnsToBookModel < ActiveRecord::Migration
  def change

  	add_column :books, :author, :string
  	add_column :books, :bookname, :string
  	add_column :books, :image, :text
  	add_column :books, :description, :text
  end

  def down
  remove_column :books, :author
  remove_column :books, :bookname
  remove_column :books, :image
  remove_column :books, :description


  end
end
