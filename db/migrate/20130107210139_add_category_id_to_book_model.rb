class AddCategoryIdToBookModel < ActiveRecord::Migration
  def change
    add_column :books, :category_id, :integer
  end

  def down
   remove_column :books, :category_id
  end
end
