class AddRemindmeColumnToBookModel < ActiveRecord::Migration
  def change
    add_column :books, :remind_me, :integer
  end

  def down
  remove_column :books, :remind_me
  end
end
