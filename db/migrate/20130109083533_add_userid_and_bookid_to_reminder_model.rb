class AddUseridAndBookidToReminderModel < ActiveRecord::Migration
  def change
  	add_column :reminders, :user_id, :integer
  	add_column :reminders, :book_id, :integer 
  end

  def down
   remove_column :reminders, :user_id
   remove_column :reminders, :book_id
  end
end
