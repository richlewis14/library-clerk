class Category < ActiveRecord::Base

  has_many :books

  attr_accessible :name
  # attr_accessible :title, :body

  def self.categories_list
  joins(:books).
  select('categories.id, categories.name, count(*) AS books_count').
  group('categories.id, categories.name').
  order('books_count DESC')
  end

def self.user_categories_list(user)
  joins(:books).
  where('books.user_id = ?', user.try(:id) || user).
  select('categories.id, categories.name, count(*) AS books_count').
  group('categories.id, categories.name').
  order('books_count DESC')
end
  
end
