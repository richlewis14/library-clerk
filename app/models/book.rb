class Book < ActiveRecord::Base
  belongs_to :user
  belongs_to :category
  has_many :reminders
  attr_accessible :author, :bookname, :image, :description, :checked_out, :user_id, :category_id, :remind_me, :avatar
  
  has_attached_file :avatar, :styles => { :medium => "152x200#", :thumb => "100x100#" }

after_save :reminder_mailer

def reminder_mailer
  if !self.checked_out && self.changes[:user_id] && self.user.nil?
  	# get all users waiting for a remind on this book
  	@users = self.reminders.map(&:user)
  	@users.inspect
  	@users.each do |user|
    ReminderMailer.remind_email(user).deliver
    end
    # remove reminders as users have been reminded
    self.reminders.destroy_all
  end
end

#Different Notice message depenedent upon Check In or Check out
def check_message
  self.checked_out ? 'You have checked out the book' : 'You have checked the book back in'
end


#Search books by name
def self.search(search)
  if search
    find(:all, :conditions => ['bookname LIKE ?', "%#{search}%"])
  else
    find(:all)
  end
end



#Scopes
#scope :checked_out_book, where(:user_id => current_user.id && :checked_out => true)
scope :member_book, lambda{|user| { :conditions => { :user_id => user.id, :checked_out => true } } }
#scope :member_book, lambda {|user| where(:user_id => user.id, :checked_out => true) }
end

