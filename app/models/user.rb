class User < ActiveRecord::Base

  has_many :books
  has_many :reminders
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me, :admin, :name, :book_id, :reminder_id
  # attr_accessible :title, :body
  
  def is_admin?
   self.admin == 1
  end
end
