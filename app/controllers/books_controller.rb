class BooksController < ApplicationController
  before_filter :authenticate_admin_user!, :except => ['index', 'show', 'update']
  include Book::BookFinder

  def index
   @books = Book.all
   @books = Book.search(params[:search])
   @bookcategories = Category.categories_list

  end

  def show
   @book = Book.find(params[:id])
   @reminder = Reminder.new
   @bookcategories = Category.categories_list
  end

  def update
    @book = Book.find(params[:id])

     if @book.update_attributes(params[:book]) 
      redirect_to books_path, :notice => @book.check_message
     else
      render :action => 'show'
     end
  end


  def results
   results = book_search(params[:search])
   @results = results
   @book = Book.new
   unless @results.empty? or @results.nil? 
   @book.author = results.first["artistName"]
   @book.bookname = results.first["trackName"]
   @book.image = results.first["artworkUrl100"].gsub("100x100-75.jpg", "200x200-75.jpg")
   @book.description = results.first["description"]
   puts @results
 end
  end

def create
  @book = Book.new(params[:book])
  if @book.save
    redirect_to searchbook_path, notice: 'Book was successfully saved'
  else
    render :action => 'results'
  end
end

  def destroy
    @book = Book.find(params[:id])
    @book.destroy
    redirect_to books_path, :notice => "Successfully Deleted Book"
  end


end