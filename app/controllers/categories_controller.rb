class CategoriesController < ApplicationController
  
  def show
    @category = Category.where(:name => params[:id]).first
    @categorisedbooks = @category.books #get me all books that have the category name
  end

  
end
