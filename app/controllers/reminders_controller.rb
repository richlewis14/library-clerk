class RemindersController < ApplicationController

	def create

		@reminder = Reminder.new(params[:reminder])
		if @reminder.save
			redirect_to books_path, notice: 'Reminder has been set'
		end
end

end
