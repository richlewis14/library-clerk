class ReminderMailer < ActionMailer::Base
  default from: "richlewis14@gmail.com"

  def remind_email(user)
    @user = user
    @url = 'http://localhost:3000'
    mail(:to => user.email, :subject => "Library Clerk")

  end

  
end
