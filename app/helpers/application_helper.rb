module ApplicationHelper

	def resource_name
    :user
  end
 
  def resource
    @resource ||= User.new
  end
 
  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end

  def remove_tags(object)
		toberemoved = ["<p>", "</p>", "<b>", "</b>", "<i>", "</i>", "<ul>", "</ul>", "<li>", "</li>", "<br />", "&nbsp;"]
		object.gsub(Regexp.union(toberemoved), '')
  end

  def login_status
    if current_user
      link_to "Sign Out", destroy_user_session_path, confirm: 'Are you sure you want to logout?', :method => :delete 
    end
  end

  def create_account 
    if !current_user
      link_to "Create Account", new_registration_path(resource_name)
    end
  end

  def all_books
    if current_user 
      link_to "All Books", books_path
    end
  end

  def my_books
    if current_user
      link_to "My Books", my_books_path
    end
  end

  def add_books
    if current_user
      link_to "Add Books", searchbook_path
    end
  end

  def image_choice
    if @book.image 
      tag("img", :src => @book.image )
    else
      image_tag @book.avatar.url(:medium)
    end
  end

  
end
